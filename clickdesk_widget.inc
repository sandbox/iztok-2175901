<?php

/**
 * @file
 * @category clickdesk
 * @package  clickdesk
 * @author
 * @license
 * @link
 */


/**
 * clickdesk_widget_form
 *
 * This function is returning a form
 *
 * for managing the widget id
 *
 * @return fixed
 *   return a form array
 */
function clickdesk_widget_form() {

  if (!user_access('administer')) {

    return FALSE;

  }

  $livily_server_url = "http://wp-1.contactuswidget.appspot.com/";

  $livily_dash = $livily_server_url . "widgets.jsp?wpurl=";

  $path = "http://" . $_SERVER['HTTP_HOST'] . "" . request_uri();

  $path = urlencode($path);

  $cdurl = $livily_dash . $path;

  $query_parameters = drupal_get_query_parameters();

  if (!empty($query_parameters['cdwidgetid'])) {
    variable_set('clickdesk_widget', $query_parameters['cdwidgetid']);
  }

  $form['clickdesk_textfield'] = array(
    '#type' => 'textfield',
    '#title' => check_plain(t('Widget Id')),
    '#default_value' => variable_get('clickdesk_widget', ''),
    '#description' => check_plain(t('Widget Id')),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $form['details'] = array(
    '#type' => 'fieldset',
    '#title' =>check_plain(t('Please wait for the page to completely load below. Signup/Login to setup.')),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#value' =>  ('<iframe width="102%" border="0" height="1300px" src="' . $cdurl . '" scrolling="yes"></iframe>'),
  );

  return $form;

}

/**
 * clickdesk_widget_text
 *
 * This function is returning a string
 *
 * @return string
 *   the link of drupal handbook
 */
function clickdesk_widget_text() {

  return t('This is a set of ten form tutorials tied to the <a href="http://drupal.org/node/262422">Drupal handbook</a>.');

}

/**
 * clickdesk_widget_form_submit
 *
 * This function is storing the clickdesk widget id
 */
function clickdesk_widget_form_submit($form, &$form_state) {

  if (!user_access('administer')) {

    return FALSE;

  }

  variable_set('clickdesk_widget', check_plain($form_state['values']["clickdesk_textfield"]));

}
