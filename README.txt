### ABOUT

  This module installs ClickDesk live chat to the Drupal 
powered websites with just a click.
  Current Features:
* Supports Drupal6 and one click installation
      * Signup, login and setup account directly without 
leaving the Drupal admin area
* Instant activation

### INSTALLATION
1. Extract ClickDesk zip/tarball files into your modules 
directory from your control panel or FTP
2. Login to your Drupal site and navigate to Administrator 
-> Site building -> Modules. Enable ClickDesk
3. Go to 'ClickDesk' link from your sidebar and wait for 
the page to completely load.
4. Signup, login and setup account directly without leaving 
the Drupal admin area
5. The widget id field gets automatically filled upon 
clicking the "Install Plugin"
 6. Click on submit and you can find ClickDesk appear 
on your website!

### CREDITS

  Official ClickDesk module. Developed and maintained by
 ClickDesk Corp - http://clickdesk.com
